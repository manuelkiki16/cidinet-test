# Título del Proyecto

_Cidinet test_




### Tecnologias usadas 📋

_NEST.JS_
_MY SQL_


### Instalación 🔧

_Para correr el api de forma local necesitamos tener instalado node js y mysql_

_El primer paso seria clonar el proyecto en git_

```
git clone https://gitlab.com/manuelkiki16/cidinet-test
```

_ve a la carpeta creada e instala las dependencias_

```
cd nombrecarpetacreada  && npm install 

```
_Abre el proyecto con editor de archivos y y abre el archiv app.modules.ts que esta en el directorio /src/app.modules.ts_

_Cambia los datos de Mysql por los datos de tu configuracion local_

```
  port: 3306,
  host: '127.0.0.1',
  username: 'dev',
  password: '1234',

```
_Ingresa a mysql y crea una base de datos llamado cidinet_employes

```
CREATE SCHEMA cidinet_employes;


```

## Despliegue 📦

Ve a la raiz del proyecto y ejecuta el siguente comando para iniciar el api

```
npm start

```
NOTA:El api corre el el puerto 3200 si lo tienes ocupado puedes cambiarlo en el archivo /src/main.ts
si lo cambias alli tambien te tocaria cambiarlo en el front end




## Construido con 🛠️


* [NESTJS](https://nestjs.com/) - Nest.js
* [TYPESCRIPT](https://www.typescriptlang.org/) - Typescript





