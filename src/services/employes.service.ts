import { Injectable } from '@nestjs/common';
import { CreateUserDto } from 'src/dto/create.employe.dto';
import { Connection } from 'typeorm';
import {Employes} from "../models/employes";

@Injectable()
export class EmployeService {

  constructor(private connection: Connection) {}


  async GetUsersMails(firstName:string,firstSurname:string){

    return this.connection.getRepository(Employes)
     .createQueryBuilder("employe")
     .where("employe.employeFirstName LIKE :firstName and employe.employeFirstSurname LIKE :firstSurname",
     {firstName:`%${firstName}%`,
       firstSurname:`%${firstSurname}%`
     }).select("employe.employeEmail")
     .orderBy("employe.employeEmail","DESC")
     .getRawMany();
    
    

  }


  async getEmployeById(employeId:number){

    return this.connection.getRepository(Employes)
    .createQueryBuilder("employe")
    .where("employe.employeId  = :employeId",{employeId}).getOne();
  }

  async ExistUserType (typeId:string,employeNumberId:string){

   return  this.connection.getRepository(Employes)
    .createQueryBuilder("employe")
    .where("employe.employeIdType = :typeId AND employe.employeNumberId = :employeNumberId",{typeId,employeNumberId})
    .getOne();
  }

  async InsertUser(userData:CreateUserDto) {

 let res = await this.connection.createQueryBuilder()
    .insert()
    .into(Employes)
    .values(userData).execute();

    return res.identifiers[0];
}
async EditUser(userData:CreateUserDto) {

  return  await this.connection.createQueryBuilder()
     .insert()
     .update(Employes)
     .set(userData)
     .where("employeId  = :employeId",{employeId:userData.employeId})
     .execute();
 }
 


  async GetEmployesList(){

    return await this.connection.getRepository(Employes)
    .createQueryBuilder("employe")
    .getMany();

  }

  async DropEmploye(employeId:number){

    return await this.connection.createQueryBuilder()
    .delete()
    .from(Employes)
    .where("employeId = :employeId", { employeId })
    .execute();

  }
}
