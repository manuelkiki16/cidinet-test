export const TYPES_IDS = {
    CC:"Cédula de Ciudadanía",
    CE:"Cédula de Extranjería",
    PP:"Pasaporte",
    PE:"Permiso Especial"
}
export const COUNTRYES = {
     COL:"Colombia",
     EU:"Estados Unidos"
}
export const AREAS_P = {
 ADM:"Administración",
 FN:"Financiera",
 CMP:"Compras",
 INF:"Infraestructura",
 OP:"Operación",
 TH:"Talento Humano",
 SV:"Servicios Varios"
}
export const DOMAIN_EMAIL="cidenet.com.co";