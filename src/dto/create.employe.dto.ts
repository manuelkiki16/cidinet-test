import { IsAlphanumeric, IsDateString, IsEmail, IsEmpty, IsIn, IsNotEmpty, IsOptional, IsString, MaxLength } from 'class-validator';
import EmployesInt from 'src/interfaces/employes.int';
import { AREAS_P, COUNTRYES, TYPES_IDS } from 'src/utils/contans';
import {IsOnlyTest} from "../validations/IsOnlyText";
import {IsValidDateIn} from "../validations/IsValidDateIn";

export class CreateUserDto implements EmployesInt {
   
  

    
    @IsOptional()
    employeId: number;

    @IsNotEmpty()  @IsOnlyTest({message:"solo permite letras"}) @IsString() @MaxLength(20)
    employeFirstName: string;

    @IsOptional()  @IsOnlyTest({message:"solo permite letras"})  @MaxLength(50,{always:false})
    employeOtherName: string;

    @IsNotEmpty() @IsString() @IsOnlyTest({message:"solo permite letras"}) @MaxLength(20)
    employeFirstSurname: string;

    @IsNotEmpty() @IsString() @IsOnlyTest({message:"solo permite letras"})  @MaxLength(20)
    employeSecondSurname: string;

    @IsNotEmpty() @IsIn(Object.keys(COUNTRYES))
    employeCountry: string;

    @IsNotEmpty() @IsIn(Object.keys(TYPES_IDS))
    employeIdType: string;

    @IsNotEmpty() @IsAlphanumeric()
    employeNumberId: string;

    @IsOptional()
    employeEmail: string;

     @IsNotEmpty() @IsDateString() @IsValidDateIn({message:"the date is not allowed"})
    employeDateIn: string;
    @IsOptional()
    emplayeDateCreated: string;

    @IsNotEmpty() @IsIn(Object.keys(AREAS_P))
    employeArea: string;
    @IsOptional()
    employeStatus: boolean;

}