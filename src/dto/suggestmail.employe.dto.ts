import { IsAlphanumeric, IsDateString, IsEmail, IsEmpty, IsIn, IsNotEmpty, IsNumber, IsOptional, IsString, MaxLength } from 'class-validator';
import EmployesInt from 'src/interfaces/employes.int';
import { IsOnlyTest } from 'src/validations/IsOnlyText';

export class SuggestMailDto implements EmployesInt {
    @IsOptional() 
    employeId: number;

    @IsNotEmpty()  @IsOnlyTest({message:"solo permite letras"}) @IsString() @MaxLength(20)
    employeFirstName: string;

    @IsOptional() 
    employeOtherName: string;
    @IsNotEmpty()  @IsOnlyTest({message:"solo permite letras"}) @IsString() @MaxLength(20)
    employeFirstSurname: string;
    @IsOptional() 
    employeSecondSurname: string;
    @IsOptional() 
    employeCountry: string;
    @IsOptional() 
    employeIdType: string;
    @IsOptional() 
    employeNumberId: string;
    @IsOptional() 
    employeEmail: string;
    @IsOptional() 
    employeDateIn: string;
    @IsOptional() 
    emplayeDateCreated: string;
    @IsOptional() 
    employeArea: string;
    @IsOptional() 
    employeStatus: boolean;
    
}