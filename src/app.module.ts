import { Module } from '@nestjs/common';
import { EmployeController } from './controllers/employes.controller';
import { EmployeService } from './services/employes.service';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Employes } from './models/employes';



@Module({
  imports: [
    TypeOrmModule.forRoot(
      {
        type: 'mysql',
        port: 3306,
        host: '127.0.0.1',
        username: 'dev',
        password: '1234',
/*         username: 'root',
        password: '1234', */
        database: 'cidinet_employes',
        entities: [Employes],
        synchronize: true,
      }
    ),

  ],
  controllers: [EmployeController],
  providers: [EmployeService],
})
export class AppModule {}
