export default interface EmployesInt{
    employeId:number;
    employeFirstName:string;
    employeOtherName:string;
    employeFirstSurname:string;
    employeSecondSurname:string;
    employeCountry:string;
    employeIdType:string;
    employeNumberId:string;
    employeEmail:string;
    employeDateIn:string;
    emplayeDateCreated:string;
    employeArea:string;
    employeStatus:boolean;
}