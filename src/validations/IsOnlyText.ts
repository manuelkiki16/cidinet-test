import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator';

export function IsOnlyTest(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'IsOnlyTest',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          const [relatedPropertyName] = args.constraints;
          const relatedValue = (args.object as any)[relatedPropertyName];
          var regexpVowels = new RegExp(/^[a-zA-Z ]+$/,'g');
          return regexpVowels.test(value);
        },
      },
    });
  };
}