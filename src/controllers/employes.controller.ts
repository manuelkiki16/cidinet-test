import { Body, Controller, Get, HttpException, HttpStatus, Param, Post,ParseIntPipe } from '@nestjs/common';
import { exists } from 'fs';
import { CreateUserDto } from 'src/dto/create.employe.dto';
import { EditUserDto } from 'src/dto/edit.employe.dto';
import { GenerateMail } from 'src/utils/globalMethos';
import { EmployeService } from '../services/employes.service';
import {AREAS_P,COUNTRYES,TYPES_IDS} from "../utils/contans";
import * as moment from 'moment';
import { SuggestMailDto } from 'src/dto/suggestmail.employe.dto';

@Controller("employes")
export class EmployeController {
  constructor(private readonly employeService: EmployeService) {}

  @Post("/create")
 async InsertUser(@Body() userData:CreateUserDto) {
 
      // let result = await this.employeService.InsertUser(userData);
   
      userData.employeFirstName = userData.employeFirstName.toLowerCase();
      userData.employeFirstSurname = userData.employeFirstSurname.toLowerCase();
      userData.employeOtherName = userData.employeOtherName?.toLowerCase();
      userData.employeSecondSurname = userData.employeSecondSurname.toLowerCase();
     
      let ExistUser  = await this.employeService.ExistUserType(userData.employeIdType,userData.employeNumberId);
   
      if (ExistUser){

        return new HttpException({
          status:400,
          msg:`The employed with ID: ${userData.employeIdType}: ${userData.employeNumberId} already registered`
        },HttpStatus.BAD_REQUEST);

      }else{

        let mailsExist = await this.employeService.GetUsersMails(userData.employeFirstName,userData.employeFirstSurname) as any;
     
        let generateMail = GenerateMail(
          userData.employeFirstName,
          userData.employeFirstSurname,
          mailsExist
        );
        userData.employeEmail = generateMail;
        console.log(userData,mailsExist,userData.employeFirstName,userData.employeFirstSurname);
    
       await this.employeService.InsertUser(userData);

       return {
         status:200,
         msg:"Employed registered successfully"
       }

      }

    
  }


  @Get("/dropEmploye/:employeId")
  async DropEmploye(@Param("employeId",ParseIntPipe) employeId:number){
    
    let existsEmploye = await this.employeService.getEmployeById(employeId);

    if( !existsEmploye ){
      return new HttpException({
        status:400,
        msg:"The employed doesnt exist in database"
      },HttpStatus.BAD_REQUEST);
    
    }
    await this.employeService.DropEmploye(employeId);

    return {
      status:200,
      msg:"The employed was deleted from database successfull "
    }



  }

  @Post("/EditUser")
  async editEmploye(@Body() userData:EditUserDto){
    

  userData.employeFirstName = userData.employeFirstName.toLowerCase();
  userData.employeFirstSurname = userData.employeFirstSurname.toLowerCase();
  userData.employeOtherName = userData.employeOtherName?.toLowerCase();
  userData.employeSecondSurname = userData.employeSecondSurname.toLowerCase();
  userData.emplayeDateCreated = moment().format('YYYY-MM-DD hh:mm:ss');

  userData.employeStatus = true;

  let actualEmployeData = await this.employeService.getEmployeById(userData.employeId);
  let ExistUser  = await this.employeService.ExistUserType(userData.employeIdType,userData.employeNumberId);

  if( !actualEmployeData  ){
    return new HttpException({
      status:400,
      msg:"The employed doesnt exist in database"
    },HttpStatus.BAD_REQUEST);
  }
  if(ExistUser && ExistUser.employeId !== actualEmployeData.employeId ){
    
    return new HttpException({
      status:400,
      msg:`The employed with ID: ${userData.employeIdType}: ${userData.employeNumberId} already registered`
    },HttpStatus.BAD_REQUEST);
    
  }

  let mailsExist = await this.employeService.GetUsersMails(userData.employeFirstName,userData.employeFirstSurname);

  let generateMail = GenerateMail(
    userData.employeFirstName,
    userData.employeFirstSurname,
    mailsExist
  );
  userData.employeEmail = generateMail;
  await this.employeService.EditUser(userData);

  return {
    status:200,
    msg:"Employed edited successfull"
  }



    

  }
  @Post("/suggestMail")
  async GetSuggestMail(@Body() userNames:SuggestMailDto){
   
    let mailExists = await this.employeService.GetUsersMails(userNames.employeFirstName,userNames.employeFirstSurname);

    let generateMail = GenerateMail(
      userNames.employeFirstName,
      userNames.employeFirstSurname,
      mailExists
    );

    return {
      status:200,
      value:generateMail
    }

  }

  @Get("/getAll")
  async getEmployes (){

    let list = await this.employeService.GetEmployesList();

    return {
      status:200,
      value:list
    }
}

 @Get('/byId/:employeId')
async GetEmployeById( @Param('employeId',ParseIntPipe) employeId:number ){


  return {
    status:200,
    value: await this.employeService.getEmployeById(employeId)
  }

 }

  @Get("/typesData")
  GetTypesData(  ){

    return {
      status:200,
      value:{
        areas:AREAS_P,
        types:TYPES_IDS,
        countries:COUNTRYES
      }
    }

  }



}
