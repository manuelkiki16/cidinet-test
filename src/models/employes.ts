import EmployesInt from 'src/interfaces/employes.int';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Employes  implements EmployesInt{


  @PrimaryGeneratedColumn()
  employeId: number;

  @Column({
    length:20
  })
  employeFirstName: string;

  @Column({
      length:50,
      nullable:true
  })
  employeOtherName: string;
  @Column({
    length:20,
  })
  employeFirstSurname: string;
  @Column({
    length:20
  })
  employeSecondSurname: string;

  @Column({
    length:50
  })
  employeIdType: string;

  @Column({
    length:50
  })
  employeCountry: string;


  @Column({
    length:20
  })
  employeNumberId: string;
  @Column({
    length:300,
    unique:true
  })
  employeEmail: string;
  @Column({
    type:"datetime",
    default:()=>'CURRENT_TIMESTAMP',
    nullable:true
  })
  emplayeDateCreated: string;
  @Column({
    type:"date",
  })
  employeDateIn: string;
  @Column({
    length:200
  })
  employeArea: string;
  @Column({
    type:"boolean",
    default:1,
    nullable:true
  })
  employeStatus: boolean;


}